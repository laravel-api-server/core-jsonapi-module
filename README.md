# Authorization Module for the API-Server
This project holds the Authorization module for the API-Server. It supports the modularization of the server.

## Installation
This module provides a custom Gate class. So please remove the `Illuminate\Auth\AuthServiceProvider::class`-provider from `config/app.php`
first.

Then activate this module by adding the `ModuleServiceProvider` to the **top**
of your `providers`-array in your `config/app.php`:
```
ApiServer\Core\\ModuleServiceProvider::class,
```

## Checking gate policies for guests
The custom Gate class makes policies beeing checked for non-authenticated users
too. See GateServiceProvider for details.

## Documentation
 * [Technology stack](doc/technology.md)
 * [Unittesting](doc/unittesting.md)
 * [Creating a release](doc/release.md)

# Contributing
## Submitting patches
Patches can be submitted using the Merge-Request link of our gitlab.

# License
See [License](LICENSE.txt)
