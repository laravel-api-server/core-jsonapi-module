## Unittesting
From within a working ApiServer server installation run:
```
cd vendor/netmon-server/models
phpunit --bootstrap ../../../bootstrap/autoload.php
```
