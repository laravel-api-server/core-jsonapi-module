<?php

namespace ApiServer\CoreJsonApi\Http\Controllers;

use ApiServer\Authentication\Exceptions\Exceptions\UnauthorizedException;

use ApiServer\JsonApi\Http\Controllers\DocumentResourceController;

class MeController extends DocumentResourceController
{
    public function model() {
      return 'ApiServer\Core\Models\User';
    }

    public function serializer() {
        return 'ApiServer\CoreJsonApi\Serializers\UserSerializer';
    }

    public function resource() {
        return "me";
    }

    /**
     * Show
     *
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $user = \Auth::user();
        if (empty($user->id)) {
            throw new UnauthorizedException(trans('api.unauthenticated'));
        }

        $user->load($this->includes);

        //return new document
        return $this->documentResponse($user);
    }
}
