<?php

namespace ApiServer\CoreJsonApi\Http\Controllers;

use ApiServer\JsonApi\Http\Controllers\DefaultResourceController;

class OptionsController extends DefaultResourceController
{
    public function model() {
      return 'ApiServer\Core\Models\Option';
    }

    public function serializer() {
        return 'ApiServer\CoreJsonApi\Serializers\ConfigSerializer';
    }

    public function resource() {
        return "configs";
    }
}
