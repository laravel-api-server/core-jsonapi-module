<?php

namespace ApiServer\CoreJsonApi\Http\Controllers;

use ApiServer\JsonApi\Http\Controllers\DefaultResourceController;

class PermissionsController extends DefaultResourceController
{
    public function model() {
      return 'ApiServer\Core\Models\Permission';
    }

    public function serializer() {
        return 'ApiServer\CoreJsonApi\Serializers\PermissionSerializer';
    }

    public function resource() {
        return "permissions";
    }
}
