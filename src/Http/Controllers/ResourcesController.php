<?php

namespace ApiServer\CoreJsonApi\Http\Controllers;

use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;
use Symfony\Component\HttpKernel\Exception\GoneHttpException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\LengthRequiredHttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotAcceptableHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\PreconditionFailedHttpException;
use Symfony\Component\HttpKernel\Exception\PreconditionRequiredHttpException;
use Symfony\Component\HttpKernel\Exception\ServiceUnavailableHttpException;
use Symfony\Component\HttpKernel\Exception\TooManyRequestsHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\HttpKernel\Exception\UnsupportedMediaTypeHttpException;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Gate;
use Auth;

//jsonapi
/*use Tobscure\JsonApi\Document;
use Tobscure\JsonApi\Collection;
use Tobscure\JsonApi\Resource;*/

//fractal
use League\Fractal\Manager;
use League\Fractal\Resource\Item;
use League\Fractal\Resource\Collection;
use League\Fractal\Serializer\DataArraySerializer;
use League\Fractal\Serializer\JsonApiSerializer;

class ResourcesController extends Controller
{
    /**
     * Index
     *
     * Display a listing of the resource.
     *
     * @Get("/")
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $resources = collect();

        $policyMappings = \Gate::getPolicyMappings();
        foreach($policyMappings as $modelName=>$policyName) {
            $model = new \ReflectionClass($modelName);
            $policy = new \ReflectionClass($policyName);
            $policyMethods = $policy->getMethods(\ReflectionMethod::IS_PUBLIC);
            $actions = collect();
            foreach($policyMethods as $policyMethod) {
                if(!$policyMethod->isConstructor()) {
                    $actions->push([
                        'name' => strtolower($policyMethod->getName())
                    ]);
                }
            }

            $resources->push([
                'name' => strtolower($model->getShortName()),
                'actions' => $actions
            ]);
        }

        //prepare response
        $manager = new Manager();
        $manager->setSerializer(new JsonApiSerializer("http://blaaaa"));
        $document = new Collection($resources, new \ApiServer\Core\Transformers\ResourceTransformer(), 'resources');

        //respond
        return response(
            $manager->createData($document)->toArray()
        )->setStatusCode(200);

/*        $apiCollection = new Collection(
                                $resources,
                                new \Netmon\Server\App\Serializers\ResourceSerializer
        );

        // Create a new JSON-API document with that collection as the data.
        $apiDocument = new Document($apiCollection);

    	//on success: return the newly created resource with statuscode 201
        return response()->json($apiDocument)->setStatusCode(200);*/
    }
}
