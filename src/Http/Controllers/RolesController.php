<?php

namespace ApiServer\CoreJsonApi\Http\Controllers;

use ApiServer\JsonApi\Http\Controllers\DefaultResourceController;

class RolesController extends DefaultResourceController
{
    public function model() {
      return 'ApiServer\Core\Models\Role';
    }

    public function serializer() {
        return 'ApiServer\CoreJsonApi\Serializers\RoleSerializer';
    }

    public function resource() {
        return "roles";
    }
}
