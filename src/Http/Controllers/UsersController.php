<?php

namespace ApiServer\CoreJsonApi\Http\Controllers;

use ApiServer\JsonApi\Http\Controllers\DefaultResourceController;

/**
 * User resource representation.
 *
 * @Resource("Users", uri="/users")
 */
class UsersController extends DefaultResourceController
{
    public function model() {
      return 'ApiServer\Core\Models\User';
    }

    public function serializer() {
        return 'ApiServer\CoreJsonApi\Serializers\UserSerializer';
    }

    public function resource() {
        return "users";
    }
}
