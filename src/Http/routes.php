<?php

Route::group([
    'namespace' => '\ApiServer\CoreJsonApi\Http\Controllers',
    'middleware' => 'cors'], function()
{
    Route::post('/users/signup', 'AccessController@signup');
    Route::post('/users/initiate-password-reset', 'AccessController@initiatePasswordReset');

	/**
	 * Routes using JWT auth
	 */
	Route::group(['middleware' => ['auth.jwt']], function () {
        Route::get('/me', 'MeController@show');

		Route::resource('users', 'UsersController', ['only' => [
			'update', 'destroy', 'index', 'show', 'store'
		]]);
	
        Route::get('/resources', 'ResourcesController@index');

        Route::resource('roles', 'RolesController', ['only' => [
			'index', 'show', 'store', 'update', 'destroy'
		]]);

		Route::resource('permissions', 'PermissionsController', ['only' => [
			'index', 'show', 'store', 'update', 'destroy'
		]]);
		
        Route::resource('configs', 'OptionsController', ['only' => [
			'index', 'show', 'store', 'update', 'destroy'
		]]);
	});
});
