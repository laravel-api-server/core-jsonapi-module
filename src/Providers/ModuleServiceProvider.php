<?php

namespace ApiServer\CoreJsonApi\Providers;

use Illuminate\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    public function register()
    {

    }

    public function boot()
    {
        require __DIR__ . '/../Http/routes.php';
    }
}

?>
