<?php

namespace ApiServer\CoreJsonApi\Serializers;

use ApiServer\JsonApi\Serializers\BaseSerializer;

class ConfigSerializer extends BaseSerializer
{
    protected $type = 'config';
    protected $model = 'ApiServer\Core\Models\Option';

    public function getAttributes($model, array $fields = null)
    {
        if (! ($model instanceof $this->model)) {
            throw new \InvalidArgumentException(
                get_class($this).' can only serialize instances of '.$this->model
            );
        }

        return [
            'key' => $model->key,
            'value'  => $model->value,
            'created_at'  => $this->formatDate($model->created_at),
            'updated_at' => $this->formatDate($model->updated_at)
        ];
    }

    public function getLinks($model) {
        //links to always include in the resource
        $links = [
            'self' => config('app.url')."/modules/{$model->id}",
        ];

        //links to include based permissions
        if(\Gate::allows('show', $model))
            $links['read'] = config('app.url')."/modules/{$model->id}";
        if(\Gate::allows('update', $model))
            $links['update'] = config('app.url')."/modules/{$model->id}";
        if(\Gate::allows('destroy', $model))
            $links['delete'] = config('app.url')."/modules/{$model->id}";

        return $links;
    }
}

?>
