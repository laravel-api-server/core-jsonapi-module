<?php namespace ApiServer\CoreJsonApi\Serializers;

use ApiServer\JsonApi\Serializers\BaseSerializer;
use ApiServer\Core\Models\Permission;

class PermissionSerializer extends BaseSerializer
{
    protected $type = 'permissions';

    public function getAttributes($permission, array $fields = null)
    {
        if (! ($permission instanceof Permission)) {
            throw new \InvalidArgumentException(
                get_class($this).' can only serialize instances of '.Permission::class
            );
        }

        return [
            'user_id' => $permission->user_id,
            'role_id' => $permission->role_id,
            'action_id' => $permission->action_id,
            'resource_id' => $permission->resource_id,
            'object_key' => $permission->object_key,
            'created_at' => $this->formatDate($permission->created_at),
            'updated_at' => $this->formatDate($permission->updated_at)
        ];
    }

    /**
     * @return \Tobscure\JsonApi\Relationship
     */
    protected function user($permission)
    {
        return $this->hasOne($permission, 'ApiServer\Core\Serializers\UserSerializer');
    }

    /**
     * @return \Tobscure\JsonApi\Relationship
     */
    protected function role($permission)
    {
        return $this->hasOne($permission, RoleSerializer::class);
    }
}

?>
