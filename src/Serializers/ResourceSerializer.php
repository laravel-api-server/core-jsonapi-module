<?php

namespace ApiServer\CoreJsonApi\Serializers;

class ResourceSerializer extends BasicSerializer
{
    protected $type = 'resources';

    public function getAttributes($resource, array $fields = null)
    {
        return [
            'name' => $resource['name']
        ];
    }

    public function getId($resource)
    {
        return $resource['name'];
    }
}

?>
