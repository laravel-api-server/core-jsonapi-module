<?php

namespace ApiServer\CoreJsonApi\Serializers;

use ApiServer\JsonApi\Serializers\BaseSerializer;
use ApiServer\Core\Models\Role;
use ApiServer\Core\Models\Permission;
use ApiServer\Core\Serializers\UserSerializer;

class RoleSerializer extends BaseSerializer
{
    protected $type = 'roles';

    public function getAttributes($role, array $fields = null)
    {
        if (! ($role instanceof Role)) {
            throw new \InvalidArgumentException(
                get_class($this).' can only serialize instances of '.Role::class
            );
        }

        return [
            'name' => $role->name,
            'description'  => $role->description,
            'created_at'  => $this->formatDate($role->created_at),
            'updated_at' => $this->formatDate($role->updated_at)
        ];
    }

    public function getLinks($model) {
        //links to always include in the resource
        $links = [
            'self' => config('app.url')."/roles/{$model->id}",
        ];

        //links to include based permissions
        if(\Gate::allows('show', $model))
            $links['read'] = config('app.url')."/roles/{$model->id}";
        if(\Gate::allows('update', $model))
            $links['update'] = config('app.url')."/roles/{$model->id}";
        if(\Gate::allows('destroy', $model))
            $links['delete'] = config('app.url')."/roles/{$model->id}";
        if(\Gate::allows('store', Permission::class))
            $links['add_permission'] = config('app.url')."/permissions";

        return $links;
    }

    /**
     * @return \Tobscure\JsonApi\Relationship
     */
    protected function users($role)
    {
        return $this->hasMany($role, 'ApiServer\Core\Serializers\UserSerializer');
    }

    /**
     * @return \Tobscure\JsonApi\Relationship
     */
    protected function permissions($role)
    {
        return $this->hasMany($role, PermissionSerializer::class);
    }
}

?>
