<?php

namespace ApiServer\CoreJsonApi\Serializers;

use ApiServer\JsonApi\Serializers\BaseSerializer;
use ApiServer\Core\Models\User;
use ApiServer\Core\Models\Role;
use ApiServer\Core\Models\Permission;

class UserSerializer extends BaseSerializer
{
    protected $type = 'users';

    public function getAttributes($user, array $fields = null)
    {
        if (! ($user instanceof User)) {
            throw new \InvalidArgumentException(
                get_class($this).' can only serialize instances of '.User::class
            );
        }

        return [
            'name' => $user->name,
            'email' => $user->email,
            'type' => $user->type,
            'created_at' => $this->formatDate($user->created_at),
            'updated_at' => $this->formatDate($user->updated_at)
        ];
    }

    public function getLinks($user) {
        //links to always include in the resource
        $links = [
            'self' => config('app.url')."/users/{$user->id}",
        ];

        //links to include based permissions
        if(\Gate::allows('show', $user))
            $links['read'] = config('app.url')."/users/{$user->id}";
        if(\Gate::allows('update', $user))
            $links['update'] = config('app.url')."/users/{$user->id}";
        if(\Gate::allows('destroy', $user))
            $links['delete'] = config('app.url')."/users/{$user->id}";
        if(\Gate::allows('store', Permission::class))
            $links['add_permission'] = config('app.url')."/permissions";

        return $links;
    }

    /**
     * @return \Tobscure\JsonApi\Relationship
     */
    protected function roles($user)
    {
        return $this->hasMany($user, 'ApiServer\Core\Serializers\RoleSerializer');
    }

    /**
     * @return \Tobscure\JsonApi\Relationship
     */
    protected function permissions($user)
    {
        return $this->hasMany($user, 'ApiServer\Core\Serializers\PermissionSerializer');
    }
}

?>
