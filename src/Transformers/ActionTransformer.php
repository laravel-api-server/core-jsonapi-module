<?php

namespace ApiServer\CoreJsonApi\Transformers;

use League\Fractal\TransformerAbstract;

class ActionTransformer extends TransformerAbstract
{
    /**
     * Transform object into a generic array
     *
     * @var  object
     */
    public function transform($action)
    {
        return [
            'id' => $action['name'],
            'name' => $action['name'],
        ];
    }
}
