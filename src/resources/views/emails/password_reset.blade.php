<p>Hello {{ $user->name }},</p>
<p>
    you have notified us about a forgotten password. By following
    <a href="{{$resetUrl}}{{$authToken}}">this link</a> you will be
    logged in without requiring a password. Please change your password
    after you have been logged in.
</P>
<p>Best whishes</p>
